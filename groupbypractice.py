import pandas as pd
'''Tutorial from:
https://www.tutorialspoint.com/python_pandas/python_pandas_groupby.htm
'''

ipl_data = {
    'Team':['Riders','Riders','Devils','Devils','Kings','kings','Kings','Kings',\
    'Riders','Royals','Royals','Riders'],
    'Rank':[1,2,3,4,4,4,1,1,2,4,1,2],
    'Year':[2014,2015,2014,2015,2014,2015,2016,2017,2016,2014,2015,2107],
    'Points':[876,789,863,673,741,812,756,788,694,701,804,690]
    }
df=pd.DataFrame(ipl_data)
x=df.groupby(['Team','Year']).groups
print(x)

grouped=df.groupby('Year')
'''
for name,group in grouped:
    print (name)
    print(type(grouped))
    print (group)
'''
#In the code below, I am showing that if you use the groupby function and get.group
#You have created a new dataframe that only has members you grouped by. So in this
# case, i grouped by year, so now group2014 only has data from 2014.
# It will keep the original indices though. You may want to rename them.  
group2014 = grouped.get_group(2014)
print(group2014) #note, 2014 is now missing from the dataframe
print('here I am')
print(group2014.index) #here are the rows
print('here I am again')
print(group2014.columns) #here are the colums
print(group2014['Team'][2]) #here I am accessing just one team
group2014['Team'][2]=group2014['Team'][9] #here I am renaming a single team to be the same as another slot
print(group2014)

#Here is how to just print a row, which is just a series:
#Note: ILOC works in order of the rows regardless of what the index value is!!!!
print(group2014.iloc[0])
print(type(group2014.iloc[0]))
#Now let's print all of them individually:
for index,each_row in enumerate(group2014.index):
    print('Here comes team: ',index  )
    print(group2014.iloc[index])

#Going back to the original data, you can iterate through each of the dataframes created by the "groupby" function
# in the following manner: 

grouped=df.groupby('Year')
print(grouped)
for name,group in grouped:
   print (name)
   print (group)
print('Here come a row')
print(df.loc[1])
#Here is how to add a row of data (Need to look this up)
#https://thispointer.com/python-pandas-how-to-add-rows-in-a-dataframe-using-dataframe-append-loc-iloc/
#group2014.append['Teams']['Hornets']

                   


                   