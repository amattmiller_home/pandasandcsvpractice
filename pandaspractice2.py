import re
import pandas as pd
import numpy as np

#by default, the Pandas rename method produces a new dataframe 
# as an output and leaves the original unchanged. And by default, 
# this output will be sent directly to the console. So when we 
# run our code like this, we’ll see the new dataframe with the 
# new name in the console, but the original dataframe will 
# be left the same.

df2 = pd.DataFrame({"C": [1, 2, 3], "B": [4, 5, 6]})
df2=df2.rename(index={0: "1", 1: 4, 2: 6})
df2=df2.rename(columns={"C": "x", "B": "y"})
print(df2)

mylist = ['hello','goodbye']
print(mylist)