import csv
import pandas as pd
import re
import matplotlib.pyplot as plt

foldername = "C:\\OurStuff\\GitRepositories\\pandasandcsvpractice\\"
filename = "dualtarget_LR_Y_AE.csv"
filealltogether = foldername+filename

Data=[]
with open(filealltogether) as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    for index,row in enumerate(csv_reader):
        if(index ==0):
            DateInfo =row
        elif(index ==1):
            TestConditions = row
        else:
            Data.append(row) 

#print(Data)

filename2 ="data_for_pandas.csv"

with open(foldername+filename2, 'w' ) as csvfile:
    writer = csv.writer(csvfile)
    for each_row in Data:
        writer.writerow(each_row)

Data2=[]
with open(foldername+filename2) as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    for index,row in enumerate(csv_reader):
        Data2.append(row)



df=pd.read_csv(foldername+filename2)
#values = df['EPC'].value_counts().keys().tolist()

#Some of the keys have nonletter characters. I want to remove them. 
#I am using regular expressions
for each_key in (df.keys()):
    print(each_key)
    df.rename(columns ={each_key: re.findall("\w.*",each_key)[0]}, inplace=True)

print(df.columns)
#Access just the EPC values

print('#Here I\'ll do some data analysis using the Groupby function.')
for each_column in df.columns:
    print(each_column)
  

    '''
print("*******RSSI MEAN********************")
print(df.groupby('EPC')['RSSI'].mean())    

print("*******RSSI MAX********************")
print(df.groupby('EPC')['RSSI'].max()) 

print("*******The code below does some analysis on the EPC data ********************")
print(df.groupby('EPC')['RSSI'].min()) '''
x=df.groupby('EPC')['RSSI'].min()
y=df.groupby('EPC')['RSSI'].max()
z=df.groupby('EPC')['RSSI'].mean()
columnnames = ['RSSI Min','RSSI Max', 'RSSI Mean']

print(type(x))
a=pd.concat([x,y,z],axis=1)
print(a.columns[0])
a.columns=columnnames

a.index.name = "EPC"
print(f"{a}")

b=df.groupby('EPC')
#print('Here I am printing b')
#print(b)

'''
print('*************Looping through the dataframe****************')
print('This code will show how to group by one column, the perform analysis',
'based on that grouping')
print('********************************************************')


for epc,groupdata in df.groupby('EPC'):
    print(epc)
    print(groupdata)
    print(groupdata.columns)
    print(groupdata['RSSI'].max())
    print(groupdata.index)
    xyz=groupdata['RSSI'].argmax()
    print(groupdata.iloc[xyz]['Channel'])
'''

#For each EPC I want to make a list that contains:
#The EPC, Number of reads, Max RSSi, Min RSSI, Avg RSSI, Max RSSI Channel, 
#Number of reads is the length of the EPC Column. I do this 
#by iterating through the "Groupby" list. That lets me perform the analysis
#on each EPC group

statsummaryheader =['EPC','NumReads','MaxRSSI','MaxRSSI Channel','MinRSSI','MinRSSI Channel','AvgRSSI']
statsummary =[]
statsummary.append(statsummaryheader)

for epc,groupdata in df.groupby('EPC'):
    #numreads = len(groupdata.columns())
    numreads = len(groupdata['EPC'])
    maxrssi = groupdata['RSSI'].max()
    rssimaxindex=groupdata['RSSI'].argmax()
    rssimaxchannel = groupdata.iloc[rssimaxindex]['Channel']
    minrssi = groupdata['RSSI'].min()
    rssiminindex=groupdata['RSSI'].argmin()
    rssiminchannel = groupdata.iloc[rssiminindex]['Channel']
    avgrssi = groupdata['RSSI'].mean()
    currententry = [epc,numreads,maxrssi,rssimaxchannel,minrssi,rssiminchannel,avgrssi]
    statsummary.append(currententry)

#Now I am going to print off the summary data as a CSV

filename2 ="Summary_Data.csv"



with open(foldername+filename2, 'w',newline = '\n' ) as csvfile:
    writer = csv.writer(csvfile,delimiter=',')
    writer.writerow(DateInfo)
    writer.writerow(TestConditions)
    
    for each_row in statsummary:
        print(each_row)
        writer.writerow(each_row)



summarycolumnames=statsummary[0]

del statsummary[0]

#df = DataFrame (People_List,columns=['First_Name','Last_Name','Age'])
#This is how to create a dataframe from a list, with a column name list
summarydf = (pd.DataFrame(statsummary,columns=summarycolumnames))
summarydf.set_index('EPC', inplace=True)
freqdata=summarydf['MaxRSSI Channel']
print(type(freqdata))



freqdata.plot.bar()
plt.show()

countdata=summarydf['NumReads']
countdata.plot.bar()
plt.show()

#(((((((((((((((((((((((((())))))))))))))))))))))))))
counter=0
for epc,groupdata in df.groupby('EPC'):
    length = len(groupdata['Channel'])
    print("B\n", geek.linspace(2.0, 3.0, num=5, retstep=True), "\n")
'''
Stuff From Vinod:
import pandas as pd

# Read CSV file into DataFrame df  use the time stamp as the index (because its unique for each row)
df = pd.read_csv('dualtarget_LR_Y_AE.csv', index_col="Timestamp")

print('-----------------Dataframe-----------------------------')

# print each row
for index, row in df.iterrows():
    print(f"TS[{index}] EPC[{row['EPC']}] Channel[{row['Channel']}]")

# groups all rows by EPC column and print out how many time each EPC is seen in the .csv
print('-----------------ValueCounts-----------------------------')
print(df['EPC'].value_counts())


# get values and counts of EPC as lists
values = df['EPC'].value_counts().keys().tolist()
counts = df['EPC'].value_counts().tolist()


print('-----------------STD of whole csv-----------------------------')
# standard deviation of whole csv
# Antenna    0.000000
# RSSI       6.690794
# Channel    7.095368
print(df.std())

print('-----------------EPC Channel STD dev ----------------------------')
# get standard deviation channel for each EPC 
print(df.groupby('EPC')['Channel'].std())
'''
                                                                                                      




# %%
