import re
import pandas as pd
import numpy as np

#*************Step one I need to make a simple Pandas Dataframe************

df = pd.DataFrame(
    {
        "Age":[21,25,8],
        "Name":["Hank","Nancy","Davis"],
        "Sex":["M","F","M"]
    }
)
print("This is the original dataframe: ")
print(df)

#************Now I am just interested in working with the Ages column and Names

ages = df["Age"]
#print(ages)

names = df["Name"]
#print(names)
print(df.columns)
#************Now I want to put these back together into a second dataframe
#https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.concat.html
df2 = pd.concat([ages,names],axis=1)
print(df2)


#************Now I want to rename the indices, ie have row names not just column names**********
#by default, the Pandas rename method produces a new dataframe 
# as an output and leaves the original unchanged. And by default, 
# this output will be sent directly to the console. So when we 
# run our code like this, we’ll see the new dataframe with the 
# new name in the console, but the original dataframe will 
# be left the same.
df=df.rename(index={0: "x", 1: "y", 2: "z"})
print("Here is the old df with new indices")
#df.rename(index = {0:"111",1:"222",2:"121"})
print(df)

#Now let's name the column that holds the indices
df.index.name="variables"
print(df)
#https://stackoverflow.com/questions/40454042/how-to-rename-columns-in-pandas-using-a-list

#Now let's rename all the columns using a list:
oldnames = df.columns.tolist() #Keeps our old column names as a list
newnames=["Yrs","Nombre","Gender"]
df.columns =newnames
print(df)

#Now let's change the last 2 back
#Restart Here
print(df.columns[0])
print(oldnames)
print('******Here is the test********')
print(oldnames[1:3]) #here I am taking a slice of the list

df.columns=[df.columns[0]]+oldnames[1:3]
print(df)
print(oldnames[1:3])

print('Here comes a row')
print(df.loc["x"])
print('Here comes another')
print(df.iloc[0])


#df.columns[0] = oldnames[0]
#print(df.columns)
s = pd.Series({'Corn Flakes': 100.0, 'Almond Delight': 100.0,\
    'Cinnamon Toast Crunch': 100.0, 'Cocoa Puff': 120.0})
print(s.argmin())
